<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

use App\Produto;
use App\Reserva;
use App\Mail\AvisoReserva;
use DB;

class ProdutoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // obtém os registros cadastrados na tabela candidatas
        $linhas = Produto::orderBy('modelo')->get();
        $linhas = Produto::paginate(3);

        return view('lista_intrumentos', ['linhas' => $linhas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('form_produtos', ['acao'=>1]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // obtém todos os campos do formulário
        $dados = $request->all();

        // armazena a foto em public/storage/fotos

        $path = $request->file('imagem')->store('fotos', 'public');

        // obtém o caminho da foto

        $dados['foto'] = $path;

        // insere o registro (com as definições do fillable (na Model) e com os 
        // nomes de campos do formulário idênticos ao da tabela)
        $reg = Produto::create($dados);

        if ($reg) {
            return redirect()->route('produtos.index')
                   ->with('status', 'Ok! Instrumento cadastrado com sucesso');
        } else {
            return redirect()->route('produtos.index')
                   ->with('status', 'Erro... Instrumento nao cadastrado...');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $reg = Produto::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // procura (e posiciona) no registro cujo id foi passado como parâmetro
        $reg = Produto::find($id);

        return view('form_produtos', ['reg' => $reg, 'acao' => 2]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
          // obtém os campos do form
          $dados = $request->all();

          // posiciona no registro a ser alterado
          $reg = Produto::find($id);
  
          // se alterou a foto... realiza a alteração da foto
          if (isset($dados['imagem'])) {
            // armazena a foto em public/storage/fotos
            $path = $request->file('imagem')->store('fotos', 'public');
  
            // obtém o caminho da foto
            $dados['foto'] = $path; 
  
            // obtém o caminho da foto antiga
            $antiga = $reg->foto;
            // exclui a foto antiga
            Storage::disk('public')->delete($antiga);  
          }
  
          // altera o registro com os novos dados do form
          $alt = $reg->update($dados);
  
          if ($alt) {
              return redirect()->route('produtos.index')
                     ->with('status', 'Ok! Instrumento cadastrado com sucesso');
          } else {
              return redirect()->route('produtos.index')
                     ->with('status', 'Erro... Instrumento nao cadastrado...');
          } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // posiciona no registro a ser excluído
        $reg = Produto::find($id);

        // obtém o caminho da foto
        $foto = $reg->foto;
        // exclui a foto 
        Storage::disk('public')->delete($foto);  

        if ($reg->delete()) {
            return redirect()->route('produtos.index')
                   ->with('status', 'Ok! Instrumento excluída com Sucesso');
        } else {
            return redirect()->route('produtos.index')
                   ->with('status', 'Erro... Instrumento não excluído...');
        }
    }

    public function dadosReservas() {
        // obtém os registros cadastrados na tabela candidatas
        $linhas = DB::select('select * from produtos, reservas 
        where(produtos.id = reservas.produto_id)');
    

        return view('dados_reservas', ['linhas' => $linhas]);
    }

    public function graficos() {
        $graficoreservas = Reserva::selectRaw('produtos.marca as marca, count(*) as num')
        ->join('produtos', 'reservas.produto_id', '=', 'produtos.id')
        ->groupBy('marca')
        ->get();
        return view('graficos', ['graficos' => $graficoreservas]);
    }

    public function relatorios() {

        $produtos = Produto::orderBy('modelo')->get();
         return \PDF::loadView('relatorios', ['produtos'=>$produtos])
         ->stream();
    }

    public function email($id) {
        $reg = Reserva::find($id);
        $email = $reg['emailCliente'];

        Mail::to($email)->send(new AvisoReserva($reg));

        return redirect()->route('produtos.index')
                   ->with('status', 'Ok! E-mail enviado com Sucesso');
    }

}
