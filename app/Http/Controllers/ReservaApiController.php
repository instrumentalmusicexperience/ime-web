<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reserva;

class ReservaApiController extends Controller
{
   

    public function index()
    {
        //
        $reservas = Reserva::orderBy('nomeCliente')->get();

        // var_dump($_POST);

        // return csrf_token();
        return response()->json($reservas, 200, [], JSON_PRETTY_PRINT);
    }

    
    public function create()
    {
        //
    }

    
    public function store(Request $request)
    {
        //requisitando os dados em json
        $dados = $request->all();
        

        $inc = Reserva::create($dados);

        if ($inc) {
            return response()->json([$inc], 201);
        } else {
            return response()->json(['error'=>'error_insert'], 500);
        }
    }

  
    public function show($id)
    {
        //
    }

  
    public function edit($id)
    {
        //
    }

   
    public function update(Request $request, $id)
    {
        //
    }

  
    public function destroy($id)
    {
        //
    }
}
