<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produto;

class ProdutoApiController extends Controller
{
   

    public function index()
    {
        //
        $produtos = Produto::orderBy('modelo')->get();

        return response()->json($produtos, 200, [], JSON_PRETTY_PRINT);
    }

   
    public function store(Request $request)
    {
        //
        $dados = $request->all();

        // var_dump($dados);
        
        $inc = Produto::create($dados);

        if ($inc) {
            return response()->json([$inc], 201);
        } else {
            return response()->json(['error'=>'error_insert'], 500);
        }
    }


    public function show($id)
    {
        //
        $reg = Produto::find($id);

        if ($reg) {
            return response()->json($reg, 200, [], JSON_PRETTY_PRINT);
        } else {
            return response()->json(['error'=>'notfound'], 404);
        }
    }

   
    public function update(Request $request, $id)
    {
        //
        $reg = Produto::find($id);

        if ($reg) {
            $dados = $request->all();

            $alt = $reg->update($dados);

            if ($alt) {
                return response()->json($reg, 200, [], JSON_PRETTY_PRINT);
            } else {
                return response()->json(['error'=>'not_updated'], 500);
            }

        } else {
            return response()->json(['error'=>'not_found'], 404);
        }

    }

   
    public function destroy($id)
    {
        //
        $reg = Produto::find($id);

        if ($reg) {
            if ($reg->delete()) {
                return response()->json(['msg'=>'Ok! Excluído'], 200);
            } else {
                return response()->json(['error'=>'not_destroy'], 500);
            }
        } else {
            return response()->json(['error'=>'not_found'], 404);
        }
    }
}
