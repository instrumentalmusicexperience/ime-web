<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

use App\Produto;
use App\Reserva;
use DB;

class ReservaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $linhas = Produto::orderBy('modelo')->get();

        return view('exibiinstrumentos', ['linhas' => $linhas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('exibiinstrumentos');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         // obtém todos os campos do formulário
        
        //return ($request->id_candidata);

        //validação dos dados informados
        $validateData = $request->validate([
            'nomeCliente' => 'required|min:10|max:60|regex:/ /um',
            'telefone' => 'required|min:9|max:11'
        ]);

        $dados = $request->all();
        
        // insere o registro (com as definições do fillable (na Model) e com os 
        // nomes de campos do formulário idênticos ao da tabela)
        $reg = Reserva::create($dados);

        if ($reg) {
            return redirect()->route('reservas.index')
                   ->with('status', 'Ok! Reserva realizada com sucesso.');
        } else {
            return redirect()->route('reservas.index')
                   ->with('status', 'Ocorreu um erro, sua reserva não foi efetuada!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $reg = Produto::find($id);

        return view('cad_reserva', ['reg' => $reg]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function pesquisa(Request $request) 
    {
        $reg = Produto::where('marca', 'LIKE', '%' . $request->busca . '%')->get();

        

        if (sizeof($reg) > 0) {
            return view('acha_instru', ['reg' => $reg, 'busca' => $request->busca]);
        } else {
            return redirect()->route('reservas.index')
                   ->with('status', 'Instrumentos encontrados!');
        }

    }
}
