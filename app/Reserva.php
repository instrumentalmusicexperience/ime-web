<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reserva extends Model
{
      //
      protected $fillable = ['nomeCliente', 'emailCliente', 'telefone', 'data', 'produto_id'];

      public function produto() {
          return $this->belongsTo('App\Produto', 'produto_id','id');
      }
}
