<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AvisoReserva extends Mailable
{
    use Queueable, SerializesModels;

    private $reserva;

    public function __construct($reserva)
    {
        $this->reserva = $reserva;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email_reserva', ['reserva'=>$this->reserva]);
    }
}
