<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Produto extends Model
{
      // indica os campos que podem ser inseridos na tabela (a partir do método create)
      use Notifiable;
      protected $fillable = ['modelo', 'marca', 'categoria', 'descricao', 'foto'];


}
