<?php

use Illuminate\Database\Seeder;

class produtos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('produtos')->insert([
            'modelo' => 'Telecaster',
            'marca' => 'Fender',
            'categoria' => 'Guitarras',
            'descricao' => 'Guitarra Americana'
        ]);
    }
}
