<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nomeCliente', 60);
            $table->string('emailCliente', 60);
            $table->string('telefone', 60);
            $table->string('data', 60);
            $table->integer('produto_id')->unsigned();
            $table->timestamps();

            $table->foreign('produto_id')->references('id')->on('produtos')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservas');
    }
}
