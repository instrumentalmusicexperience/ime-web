@extends('principal')
@section('pagina')

@if (session('status'))
<div class="alert alert-success">
  {{ session('status') }}
</div>
@endif

<tbody>

    <div class="row">
        <div class="col-sm-12">
            <p>
              Clique para fazer na foto para fazer a reserva
            </p>
        </div>
    </div>

@foreach ($linhas as $linha)
    
    
        <div class="col-md-4">
          <div class="thumbnail" >
            <a href='{{ route('reservas.show', $linha->id) }}'>
              <img src='storage/{{ $linha->foto }}' alt="Foto" style='width: 300px; height: 200px;'></a>
              <div class="caption">
                <p>
                  <b>Marca --</b>  {{$linha->marca}} 
                </p>
              </div>
            
          </div>
        </div>
    



@endforeach

</tbody>

@endsection