@extends('principal')
@section('pagina')

@if (session('status'))
<div class="alert alert-success">
  {{ session('status') }}
</div>
@endif
<div>
  <h3>Instrumentos encontrados</h3>
</div>


<div>
  @foreach ($reg as $linha)
    
  <div class="row">
    <div class="col-sm-6">
      <a href="{{ route('reservas.show', $linha->id) }}">
      <img src='storage/{{ $linha->foto }}' alt="foto" style='width: 400px; height: 300px;' ></a>

      <p>
        -------------------------------------------------------------------------------------
      </p>
    
    </div>
    <div class="col-sm-6">
      <h4>
       Modelo: {{$linha->modelo}}
      </h4>
      <h4>
       Marca: {{$linha->marca}}
      </h4>
    </div>
  </div>


@endforeach
</div>

@endsection