<!DOCTYPE html>
<html lang="pt-br">
<head>
  <title>IME</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
   <div class="container">
        <div class="row">
            <div class="col-sm-10">
                <h1>
                    INSTRUMENTAL MUSICAL EXPERIENCE
                </h1>
            </div>
            <div class="col-sm-2">
                    <a href="{{route('reservas.index')}}"><p style="margin-top:24px">Home</p></a>                     
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-12">
                <h3>Conheça, teste e aprove antes de comprar!</h3>
            </div>
        </div>
    <form action="{{route('reserva.pesquisa')}}" method="post">
        <div class="row">
            <div class="col-sm-8 input-group">
                    {{ csrf_field() }}
                    <input type="text" class="form-control" id="busca" name="busca" placeholder="Ache o instrumento que procura">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="submit">OK</button>
                    </span>
                </div>
            </div>
    </form>
        <div class="container">
            
            @yield('pagina')
        </div>
        
   </div>
   
</body>
</html>