@extends('modelo') 
@section('conteudo')

<div class="row">
  <div class="col-sm-12">
     <h3>Reservas Feitas</h3>
  </div>     
</div>

@if (session('status'))
<div class="alert alert-success">
  {{ session('status') }}
</div>
@endif

<table class="table table-hover">
  <thead>
    <tr>
      <th>C.R.</th>
      <th>Nome</th>
      <th>Email</th>
      <th>Tel.Celular</th>
      <th>Data Desejada</th>
      <th>C.I.</th>
      <th>Instrumento Reservado</th>
      <th>Envio de confirmação por email</th>
    </tr>
  </thead>
  <tbody>

    @foreach ($linhas as $linha)
    <tr>
      <td> {{ $linha->id }} </td>
      <td> {{ $linha->nomeCliente }} </td>
      <td> {{ $linha->emailCliente }} </td>
      <td> {{ $linha->telefone }} </td>
      <td> {{ $linha->data }} </td>
      <td> {{ $linha->produto_id }} </td>
      <td> {{ $linha->modelo }}, {{$linha->categoria}}, {{$linha->marca}} </td>
      <td> 
          <a href="{{ route('produtos.email', $linha->id) }}" class="btn btn-primary btn-sm" role="button">Enviar</a>&nbsp;


      </td>
    </tr>
    
    @endforeach

  </tbody>
</table>

{{-- {{ $linhas->links() }} --}}
  
 
@endsection