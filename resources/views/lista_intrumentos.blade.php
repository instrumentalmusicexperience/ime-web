@extends('modelo') 
@section('conteudo')

<div class="row">
  <div class="col-sm-10">
     <h3>Instrumentos Cadastrados</h3>
  </div>   
  <div class="col-sm-2">
    <a href="{{ route('produtos.create') }}" class="btn btn-primary btn-sm" style="margin-top:24px" role="button">Cadastro Novo</a>
  </div>   
</div>

@if (session('status'))
<div class="alert alert-success">
  {{ session('status') }}
</div>
@endif

<table class="table table-hover">
  <thead>
    <tr>
      <th>Código</th>
      <th>Modelo</th>
      <th>Marca</th>
      <th>Categoria</th>
      <th>Descrição</th>
      <th>Foto</th>
      <th>Ações</th>
    </tr>
  </thead>
  <tbody>

    @foreach ($linhas as $linha)
    <tr>
      <td> {{ $linha->id}} </td>
      <td> {{ $linha->modelo }} </td>
      <td> {{ $linha->marca }} </td>
      <td> {{ $linha->categoria }} </td>
      <td> {{ $linha->descricao }} </td>
      <td> <img src='../../storage/{{ $linha->foto }}' style='width: 120px; height: 80px;'> </td>
      <td> <a href="{{ route('produtos.edit', $linha->id) }}" class="btn btn-info btn-sm" role="button">Alterar</a>&nbsp;
        <!--  <a href="{{ route('produtos.show', $linha->id) }}" class="btn btn-success btn-sm" role="button">Consultar</a>&nbsp; -->
        <form method="post" action="{{ route('produtos.destroy', $linha->id)}}" style="display: inline-block" onsubmit="return confirm('Confirma Exclusão do Instrumento?')">          
          {{ method_field('delete') }}
          {{ csrf_field() }}
          <input type="submit" class="btn btn-danger btn-sm" value="Excluir">
        </form>
        <!-- <a href="{{ route('produtos.email', $linha->id) }}" class="btn btn-primary btn-sm" role="button">E-mail</a>&nbsp; -->

      </td>
    </tr>
    
    @endforeach

  </tbody>
</table>

{{ $linhas->links() }}
  
 
@endsection