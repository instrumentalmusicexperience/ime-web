@extends('modelo') 
@section('conteudo')

<div class="row">
  <div class="col-sm-12">
     <h3>Gráficos de Marcas</h3>
  </div>     
</div>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
 google.charts.load("current", {packages:["corechart"]});
 google.charts.setOnLoadCallback(drawChart);

 function drawChart() {
 var data = google.visualization.arrayToDataTable([['Marca', 'Nº Reservas'],
 @foreach ($graficos as $grafico)
 {!! "['$grafico->marca', $grafico->num]," !!}
 @endforeach
 ]);
 var options = {
 title: 'O gráfico abaixo mostra a relação das marcas de instrumentos que estão na preferência dos clientes.',
 is3D: true,
 };
 var chart = new google.visualization
 .PieChart(document.getElementById('piechart_3d'));
 chart.draw(data, options);
 }
</script>
<div id="piechart_3d" style="width: 900px; height: 500px;"></div>

@endsection