@extends('principal') 
@section('pagina') 


@if (session('status'))
<div class="alert alert-success">
    {{ session('status') }}
</div>
@endif

@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif


    <h3>Cadastro para reserva</h3>
    

        <div class="row">
            <div class="col-sm-6">
                <img src='../../storage/{{ $reg->foto }}' alt="Foto" style='width: 300px; height: 200px;' >
            </div>
            <div class="col-sm-6">
                <p>
                    Modelo: {{$reg->modelo}}
                </p>
                <p>
                    Marca: {{$reg->marca}}
                </p>
            </div>

        </div>

        <h4>Informe seus dados para concluir a Reserva</h4>
        
        <form method="post" action="{{ route('reservas.store') }}" enctype="multipart/form-data">

       
            {{ csrf_field() }}
            <div class="row">
                <div class="col-sm-8">
                    <div class="form-group">
                        <label for="nome">Nome:</label>
                        <input type="text" class="form-control" id="nomeCliente" name="nomeCliente" autofocus
                        >

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-8">
                    <div class="form-group">
                        <label for="email">Email:</label>
                        <input type="email" class="form-control" id="emailCliente" name="emailCliente">

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-8">
                    <div class="form-group">
                        <label for="email">Tel.Celular:</label>
                        <input type="text" class="form-control" id="telefone" name="telefone">

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-8">
                    <div class="form-group">
                        <label for="email">Data Desejada:</label>
                        <input type="text" class="form-control" id="data" name="data">

                    </div>
                </div>
            </div>
            <input type="hidden" id='produto_id' name='produto_id' value='{{$reg->id}}'>
            <div class="row">
                <div class="col-sm-8">
                    <input type="submit" value="Reservar" class="btn btn-danger" style="margin:10px">
                </div>
            </div>
        </form>

{{-- @endsection

@section('js') --}}
  <script src="https://code.jquery.com/jquery-latest.min.js"></script>
  <script src="{{ URL::asset('js/jquery.mask.min.js') }}"></script>

  <script>
    $(document).ready(function() {
        $('#data').mask('00/00/0000');
    });
  </script>  
@endsection
