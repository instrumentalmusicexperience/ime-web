<!DOCTYPE html>
<html lang="pt-br">
<head>
  <title>IME</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>

<body>

<div class="container-fluid">
    <h4 style="text-align: center">INSTRUMENTAL MUSICAL EXPERIENCE</h4>
    <h5 style="text-align: center">Relatório de Instrumentos Cadastrados no Sistema</h5>
    <table class="table table-striped table-sm">
        <thead class="thead-light">
            <tr>
                <th>Modelo</th>
                <th>Marca</th>
                <th>Categoria</th>
                <th>Imagem</th>
            </tr>
        </thead>
        <tbody>
            @foreach($produtos as $produto)
            <tr>
                <td>{{$produto->modelo}}</td>
                <td>{{$produto->marca}}</td>
                <td>{{$produto->categoria}}</td>
                <td style="text-align: center">
                        <img src="{{public_path('storage/'.$produto->foto)}}" style="width:100px;height:60px">
                {{-- @if (Storage::exists($candidata->foto))
                    
                @else 
                    <img style="width: 100px" src="{{public_path('storage/fotos/semfoto.jpeg')}}">
                @endif --}}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
    
</body>

</html>