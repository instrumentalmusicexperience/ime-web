# Projeto Sistema Distribuído Parte WEB - Sistemas Distribuídos


### Tecnologias Utilizadas 

*  Apache 
*  Mysql
*  Css
*  Html
*  Framework Laravel
*  PHP

### Requisitos 

*  É preciso baixar e instalar o composer. 

#### Como rodar a aplicação passo a passo

*  Baixe e instale na sua máquina os Software Xampp. A instalação destes softwares é simples como qualquer outro;
*  Se não tiver na sua máquina baixe e instale um editor de texto (recomendo o **VSCode**, na minha minha opinião um dos melhores gratuitos disponíveis e a instalação também é simples);
*  Abra o xampp e clique no **start** em Apache e Mysql.
*  No browser entre no servidor em **localhost/phpmyadmin** e crie um novo banco de dados chamado **ime**.
*  Abra o editor de texto.
*  No editor abra o projeto ime-web.
*  Pressione **control** e **apóstrofo** para abrir o terminal direto pelo editor (você pode fazer isso pelo cmd do windowns se preferir).
*  Digite o comando **php artisan migrate** para realizar as migrações.
*  Após digite o comando **php artisan serve** para startar o projeto.
*  User as rotas do projeto para testar. (localhost:8000/produtos => essa é uma delas).

### Licença Utilizada

*  Mit License


