<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('produtos', 'ProdutoController')->middleware('auth');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('reservas', 'ReservaController');

Route::post('pesquisa', 'ReservaController@pesquisa')->name('reserva.pesquisa');

Route::get('dadosReservas', 'ProdutoController@dadosReservas')->name('produtos.dadosReservas');

Route::get('graficos', 'ProdutoController@graficos')->name('produtos.graficos');

Route::get('relatorios', 'ProdutoController@relatorios')->name('produtos.relatorios');

Route::get('res_email/{id}', 'ProdutoController@email')->name('produtos.email')->middleware('auth');

Route::resource('reservasapi', 'ReservaApiController', ['except'=>['create', 'edit']]);
